import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import Header from './components/Header';
import Landing from './components/Landing'
import Dashboard from './components/Dashboard'
import SurveyNew from './components/surveys/SurveyNew'
import logo from './logo.svg';
import { connect } from 'react-redux';
import * as actions from './actions/index'

class App extends Component {
  componentDidMount() {
    this.props.fetchedUser();
  }

  render() {
    return (
      <div className="container">
        <Header />
        <Switch>
          <Route exact path="/" component={Landing} />
           <Route path="/surveys/new" component={SurveyNew} />
          <Route path="/surveys" component={Dashboard} />
         
        </Switch>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchedUser: () => dispatch( actions.fetchUser() )
  };
}

export default withRouter(connect(null, mapDispatchToProps)(App));
