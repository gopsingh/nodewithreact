import axios from 'axios';
import * as actions from './types';

export const checkUser = (payload) => {
	return {
		type: actions.FETCH_USER,
		payload: payload
	};
}

export const fetchSurvey = (payload) => {
  return {
    type: actions.FETCH_SURVEYS,
    payload: payload
  };
}

export const fetchUser = () => {
	return dispatch => {
		axios.get('/api/current_user')
			.then(res => {
				dispatch(checkUser(res.data));
			})
	};
}

export const handleToken = (token) => {
  	return dispatch => {
  		axios.post('/api/stripe', token)
  		.then(res => {
  			dispatch(checkUser(res.data));
  		})
  	};
};

export const submitSurvey = (values, history)=>  {
  return dispatch => {
    axios.post('/api/surveys', values)
    .then(res => {
      history.push('/surveys');
      dispatch(checkUser(res.data));
    });
  };
};

export const fetchSurveys = () => dispatch => {
  return dispatch => {
    axios.get('/api/surveys')
      .then(res => {
        dispatch(fetchSurvey(res.data));
      });
  }
};