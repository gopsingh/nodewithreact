// SurveyFormReview shows users their form inputs for review
import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import formFields from './formField';
import { withRouter } from 'react-router-dom';
import * as actions from '../../actions/index';

const SurveyFormReview = (props) => {
  const reviewFields = _.map(formFields, ({ name, label }) => {
    return (
      <div key={name}>
        <label>{label}</label>
        <div>
          {props.formValues[name]}
        </div>
      </div>
    );
  });

  const survey = () => {
    props.onSubmitSurvey(props.formValues, props.history);
  }

  return (
    <div>
      <h5>Please confirm your entries</h5>
      {reviewFields}
      <button
        className="yellow darken-3 white-text btn-flat"
        onClick={props.onCancel}>Back
      </button>
      <button
        onClick={survey}
        className="green btn-flat right white-text"
      >
        Send Survey
        <i className="material-icons right">email</i>
      </button>
    </div>
  );
};

function mapStateToProps(state) {
  return { formValues: state.form.surveyForm.values };
}

const mapDispatchToProps = dispatch => {
  return {
    onSubmitSurvey: (formValues, history) => dispatch( actions.submitSurvey(formValues, history) )
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SurveyFormReview));