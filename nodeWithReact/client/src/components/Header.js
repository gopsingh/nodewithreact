import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom'
import Payments from './Payments'

class Header extends Component {
	renderContent() {
		switch (this.props.auth){
			case null:
				return;
			case false:
				return <li><a href="/auth/google">LogIn with Google</a></li>;
			default:
				return [
					<li key="1"><Payments /></li>,
					<li key="3" style={{margin: '0 10px'}}>Credits: {this.props.auth.credits}</li>,
					<li key="2"><a href="/api/logout">Logout</a></li>
				];
		}
	}

	render() {
		return (
			<nav>
			    <div className="nav-wrapper">
			      <NavLink to={this.props.auth ? '/surveys' : '/'} className="brand-logo">Emaily</NavLink>
			      <ul id="nav-mobile" className="right hide-on-med-and-down">
			        {this.renderContent()}
			      </ul>
			    </div>
		  	</nav>
		);
	}
}

const mapStateToProps = state => {
	return {
		auth: state.auth
	};
}

export default connect(mapStateToProps)(Header);