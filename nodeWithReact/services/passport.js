const passport = require('passport');
const mongoose = require('mongoose');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const keys = require('../config/keys');

const User = mongoose.model('users');

passport.serializeUser((user, done) => {
	done(null, user.id);
});

passport.deserializeUser((id, done) => {
	User.findById(id).then(user => {
		done(null, user);
	});
});

passport.use(new GoogleStrategy({
	clientID: keys.googleClientID,
	clientSecret: keys.googleClientSecret,
	callbackURL: '/auth/google/callback',
	proxy: true
}, (accessToken, refreshToken, profile, done) => {
	User.findOne({googleId: profile.id}).then(existingUser => {
		if (existingUser) {
			//we already have a user
			done(null, existingUser);
		} else {
			//create a new user
			new User({googleId: profile.id}).save()
				.then(user => done(null, user));
		}
	});
}));

/*var HttpsProxyAgent = require('https-proxy-agent');
if (process.env['https_proxy']) {
  var httpsProxyAgent = new HttpsProxyAgent(process.env['https_proxy']);
  strategy._oauth2.setAgent(httpsProxyAgent);
}
passport.use(strategy);*/